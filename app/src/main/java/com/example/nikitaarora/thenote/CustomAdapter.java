package com.example.nikitaarora.thenote;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/**
 * Created by nikita.arora on 10/19/2015.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>
{
    private Context mContext;
    Cursor cursor;

    CustomAdapter(Cursor cursor, Context context)
    {
        this.cursor = cursor;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.custom_row, parent, false);

        return new ViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String noteText;
        if(cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            cursor.moveToPosition(position);
            noteText = cursor.getString(cursor.getColumnIndex(DBOpenHelper.NOTE_TEXT));

            holder.note_text.setText(noteText);

            holder.note_text.setTag(cursor.getLong(cursor.getColumnIndex(DBOpenHelper.NOTE_ID)));

            if(position == 0)
            {
                setAnimation(holder.note_text, mContext);
            }
        }
    }


    @Override
    public int getItemCount() {
        int num = 0;
        if(cursor != null){
            cursor.moveToFirst();
            num = cursor.getCount();

        }
        return num;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        protected TextView note_text;

        public ViewHolder(View itemView) {
            super(itemView);
            note_text = (TextView) itemView.findViewById(R.id.textView_note);
        }
    }

    public void changeCursor(Cursor cursor)
    {
        this.cursor = cursor;
        notifyDataSetChanged();

    }

    @Override
    public long getItemId(int position) {
        return cursor.getLong(cursor.getColumnIndex(DBOpenHelper.NOTE_ID));
    }

    private void setAnimation(View viewToAnimate, Context mContext)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
            Animation animation = AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.slide_in);
            viewToAnimate.startAnimation(animation);
    }

}


